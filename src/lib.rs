use std::error::Error as StdError;
use std::ffi::OsStr;
use std::fmt;
use std::io::{Error as IoError, Read};
use std::num::ParseFloatError;
use std::process::{Command, ExitStatus};

use tempfile::NamedTempFile;

pub fn measure_child<I, S>(args: I) -> Result<(ExitStatus, f64), Error>
where
    I: IntoIterator<Item = S>,
    S: AsRef<OsStr>,
{
    let mut tmp = NamedTempFile::new()?;

    let status = Command::new("perf")
        .arg("stat")
        .arg("--event=power/energy-pkg/")
        .arg("--output")
        .arg(tmp.path())
        .arg("--field-separator")
        .arg(",")
        .arg("--")
        .args(args)
        .env("LANG", "C")
        .status()?;

    let mut buf = String::new();
    tmp.read_to_string(&mut buf)?;

    for line in buf.lines() {
        let line = line.trim_start();

        if line.is_empty() || line.starts_with('#') {
            continue;
        }

        let mut fields = line.split(',');

        let value = fields
            .next()
            .ok_or_else(|| Error::MissingEnergyValue(line.to_owned()))?;
        let unit = fields
            .next()
            .ok_or_else(|| Error::MissingEnergyUnit(line.to_owned()))?;

        let value = value.parse::<f64>().map_err(Error::InvalidEnergyValue)?;

        if unit != "Joules" {
            return Err(Error::UnexpectedEnergyUnit(unit.to_owned()));
        }

        return Ok((status, value));
    }

    Err(Error::MissingEnergy)
}

#[derive(Debug)]
pub enum Error {
    Io(IoError),
    MissingEnergy,
    MissingEnergyValue(String),
    MissingEnergyUnit(String),
    InvalidEnergyValue(ParseFloatError),
    UnexpectedEnergyUnit(String),
}

impl From<IoError> for Error {
    fn from(err: IoError) -> Self {
        Self::Io(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Io(err) => write!(fmt, "I/O error: {}", err),
            Self::MissingEnergy => write!(fmt, "Energy missing in output of perf stat"),
            Self::MissingEnergyValue(line) => write!(fmt, "Energy value missing in line: {}", line),
            Self::MissingEnergyUnit(line) => write!(fmt, "Energy unit missing in line: {}", line),
            Self::InvalidEnergyValue(err) => write!(fmt, "Invalid energy value: {}", err),
            Self::UnexpectedEnergyUnit(unit) => write!(fmt, "Unexpected energy unit: {}", unit),
        }
    }
}

impl StdError for Error {}
