use std::env::args;
use std::error::Error;
use std::fs::OpenOptions;
use std::io::Write;
use std::os::unix::process::ExitStatusExt;
use std::process::exit;

use energize::measure_child;

fn main() -> Result<(), Box<dyn Error>> {
    let mut args = args().skip(1);
    let out = args.next().expect("Missing output file");
    let args = args.collect::<Vec<_>>();
    let cmd = args.join(" ");

    let (status, value) = measure_child(&args)?;

    let mut out = OpenOptions::new().create(true).append(true).open(out)?;

    writeln!(out, "# TYPE energy gauge")?;
    writeln!(out, "# UNIT energy joules")?;
    writeln!(out, r#"energy{{command="{cmd}"}} {value}"#)?;

    exit(status.into_raw())
}
