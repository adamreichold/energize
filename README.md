# energize

A simple wrapper around `perf stat` to capture the energy consumption of a command measured in Joules using the `power/energy-pkg` event and append the result to an [OpenMetrics](https://github.com/OpenObservability/OpenMetrics) file.

This can be used to check the energy consumption of tests or benchmarks, e.g.

```console
> export CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUNNER="energize energy.metrics"
> cargo test --test coin_flip
    Finished test [unoptimized + debuginfo] target(s) in 0.01s
     Running tests/coin_flip.rs (target/debug/deps/coin_flip-395a78f6f0a5b038)

running 1 test
test coin_flip ... ok

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 2.90s

> cargo test --test coin_flip --release
    Finished release [optimized] target(s) in 0.01s
     Running tests/coin_flip.rs (target/release/deps/coin_flip-c24a29b923beac35)

running 1 test
test coin_flip ... ok

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.08s

> cat energy.metrics
# TYPE energy gauge
# UNIT energy joules
energy{command="target/debug/deps/coin_flip-395a78f6f0a5b038"} 31.34
# TYPE energy gauge
# UNIT energy joules
energy{command="target/release/deps/coin_flip-c24a29b923beac35"} 0.75
```
